import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SurveyService {

  set_service_data:any = []
  sum: any;
  comments: any;

  constructor() { }

  set_survey(e:any, value:any){

    // const values = Object.values(e);
    
    // this.sum = values.reduce((accumulator:any, value:any) => {
    //   return parseInt(accumulator) + parseInt(value);
    // });

    const values = Object.values(e);
    console.log(values);
    
    this.sum = values.reduce((accumulator:any, value:any) => {
      return parseInt(accumulator) + parseInt(value);
    },0);
      console.log(this.sum);
      
    const average = this.sum/5  ;
    console.log(average);
    if(average<=1){
      this.comments = 'Bad'
    }
    else if(average>=1.1 && average<=2){
      this.comments ='Poor'
    }
    else if(average>=2.1 && average<=3){
      this.comments ='Average'
    }
    else if(average>=3.1 && average<=4){
      this.comments ='Good'
    }
    else if(average>=4.1 && average<=5){
      this.comments ='Very Good'
    }
    else if(average>=5.1 && average<=6){
      this.comments ='Excellent'
    }

    let data = {"sum":average, "name" : value.name,'rate' : this.comments, 'comment':value.comment}
    this.set_service_data.push(data);
 
    
    console.log(this.set_service_data);
    
  }

  get_service(){
    return this.set_service_data
  }
}
