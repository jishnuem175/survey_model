import { Component, ElementRef, OnInit, Renderer2 } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { SurveyService } from 'src/app/survey.service';
import Swal from 'sweetalert2';
import { SuccessPopMessageComponent } from '../success-pop-message/success-pop-message.component';
@Component({
  selector: 'app-feedback-modal',
  templateUrl: './feedback-modal.component.html',
  styleUrls: ['./feedback-modal.component.scss']
})
export class FeedbackModalComponent implements OnInit {

  stars: number[] = [1, 2, 3, 4, 5];
  selectedValue: any = '';

  surveyForm: any;
  name_form: any;
  errorss: any;
  feedback_data: any = [];

  constructor(private fb: FormBuilder, private svr: SurveyService,private dialog:MatDialog,  private elementRef: ElementRef, private renderer: Renderer2) { }

  ngOnInit(): void {
    this.surveyForm = this.fb.group({
      name: ['', [Validators.required]],
      valone: ['', [Validators.required]],
      valtwo: ['', [Validators.required]],
      valthree: ['', [Validators.required]],
      valfour: ['', [Validators.required]],
      valfive: ['', [Validators.required]],
      comment: ['',[Validators.required]],
    })
  }

  submit() {
    
    if (this.surveyForm.valid && this.selectedValue.length != '') {
      if(this.selectedValue.length == ''){
        Swal.fire({
          title: 'Feedback',
          html: 'Please rate the feedback',
          icon: 'error'
        });
      }

      let valone = this.surveyForm.value.valone;
      let valtwo = this.surveyForm.value.valtwo;
      let valthree = this.surveyForm.value.valthree;
      let valfour = this.surveyForm.value.valfour;
      let valfive = this.surveyForm.value.valfive;
      let val = {'valone':valone,'valtwo':valtwo,'valthree':valthree, 'valfour':valfour, 'valfive':valfive}
      console.log(val);
      let val_name = this.surveyForm.value.name;
      
      let val_data = {'name':val_name, 'comment': this.surveyForm.value.comment}

      this.svr.set_survey(val, val_data)
      this.dialog.open(SuccessPopMessageComponent,
        {
          width:'50vw',
          maxWidth:'100%'
        })

        this.surveyForm.value.name = ''
    } else {
      Swal.fire({
        title: 'Feedback',
        html: 'Please add all the feedback',
        icon: 'error'
      });

      // if(this.selectedValue.length == ''){
      //   Swal.fire({
      //     title: 'Feedback',
      //     html: 'Please rate the feedback',
      //     icon: 'error'
      //   });
      // }
    }

  }

  Preview_onClick(form:any){
    if (form.invalid) {
      return form.markAllAsTouched();
     } else {
      console.log("success");
      
     }
  }

  countStar(star: any) {
    this.selectedValue = star;
    console.log('Value of star', star);
  }

  // get myForm_name() {
  //   return this.surveyForm.get('name'); 
  // }

  get myForm_vali() {
    return this.surveyForm.controls;
  }

  goToBottom(){
    const dialogContainer = this.elementRef.nativeElement.querySelector('.container-fluid');
    dialogContainer.scrollTop = dialogContainer.scrollHeight;
  }

 
}
