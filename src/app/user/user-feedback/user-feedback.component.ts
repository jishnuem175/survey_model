import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FeedbackModalComponent } from '../feedback-modal/feedback-modal.component';

@Component({
  selector: 'app-user-feedback',
  templateUrl: './user-feedback.component.html',
  styleUrls: ['./user-feedback.component.scss']
})
export class UserFeedbackComponent implements OnInit {

  constructor(private dialog:MatDialog) { }

  ngOnInit(): void {
    this.dialog.open(FeedbackModalComponent, {
      width:'75vw',
      maxWidth:'100%',
      autoFocus: false,
      // height:'80dvh',
      // maxHeight:'100%'
    })
  }

  open_feedback(){
    this.dialog.open(FeedbackModalComponent, {
      width:'75vw',
      maxWidth:'100%',
      autoFocus: false,
      // height:'80dvh',
      // maxHeight:'100%'
    })
  }

}
