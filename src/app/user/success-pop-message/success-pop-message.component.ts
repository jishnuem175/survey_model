import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-success-pop-message',
  templateUrl: './success-pop-message.component.html',
  styleUrls: ['./success-pop-message.component.scss']
})
export class SuccessPopMessageComponent implements OnInit {

  constructor(private dialog:MatDialog) { }

  ngOnInit(): void {
  }

  close_modal(){
    this.dialog.closeAll()
  }

}
