import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuccessPopMessageComponent } from './success-pop-message.component';

describe('SuccessPopMessageComponent', () => {
  let component: SuccessPopMessageComponent;
  let fixture: ComponentFixture<SuccessPopMessageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SuccessPopMessageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SuccessPopMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
