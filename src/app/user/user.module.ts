import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { UserFeedbackComponent } from './user-feedback/user-feedback.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';
import { FeedbackModalComponent } from './feedback-modal/feedback-modal.component';
import {MAT_DIALOG_DATA, MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import {MatRadioModule} from '@angular/material/radio';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { SuccessPopMessageComponent } from './success-pop-message/success-pop-message.component'
@NgModule({
  declarations: [
    UserFeedbackComponent,
    FeedbackModalComponent,
    SuccessPopMessageComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    MatDialogModule,
    MatRadioModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class UserModule { }
