import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  hide=true;
  user_name:any = 'admin@gmail.com';
  password:any = '1234';

  constructor(private route:Router) { }

  ngOnInit(): void {
  }

  login(){
    if(this.user_name == 'admin@gmail.com'){
      this.route.navigate(['/admin'])
    }else{
      this.route.navigate(['/user'])
    }
  }

}
