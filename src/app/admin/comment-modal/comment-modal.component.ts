import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-comment-modal',
  templateUrl: './comment-modal.component.html',
  styleUrls: ['./comment-modal.component.scss']
})
export class CommentModalComponent implements OnInit {
  comment: any;

  constructor(@Inject(MAT_DIALOG_DATA) public data:any) { 
    console.log(data);
    this.comment = data;
  }

  ngOnInit(): void {
    this.comment = this.comment;
  }

}
