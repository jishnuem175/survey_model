import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SurveyService } from 'src/app/survey.service';
import { ViewModalComponent } from '../view-modal/view-modal.component';

@Component({
  selector: 'app-admin-view',
  templateUrl: './admin-view.component.html',
  styleUrls: ['./admin-view.component.scss']
})
export class AdminViewComponent implements OnInit {

  constructor(private svr : SurveyService, private dialog:MatDialog) { }

  ngOnInit(): void {
    this.open_view()
    this.get_survey_data()
  }

  get_survey_data(){
    let data = this.svr.get_service()
    console.log(data);
    
  }

  open_view(){
    this.dialog.open(ViewModalComponent, {
      width:'45vw',
      maxWidth:'100%',
      // height:'80dvh',
      // maxHeight:'100%'
    })
  }


}
