import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { SurveyService } from 'src/app/survey.service';
import { CommentModalComponent } from '../comment-modal/comment-modal.component';

@Component({
  selector: 'app-view-modal',
  templateUrl: './view-modal.component.html',
  styleUrls: ['./view-modal.component.scss']
})
export class ViewModalComponent implements OnInit {

  displayedColumns: string[] = [
    'name',
    'rating',
    'comment',
    'view'
  ];
  dataSource!: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  comment: any;

  constructor(private svr: SurveyService, private dialog:MatDialog) { }

  ngOnInit(): void {

    this.get_survey_data()
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  get_survey_data() {
    let data = this.svr.get_service()
    console.log(data);
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  head_selected(row:any){
    console.log(row.comment);
    this.comment = row.comment;
    this.ope_comment()
  }

  ope_comment(){
    this.dialog.open(CommentModalComponent,{
      data:this.comment,
      width:'35vw',
      maxWidth:'100%'
    })
  }
}
